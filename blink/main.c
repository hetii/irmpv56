#include <avr/io.h>
#include <util/delay.h>


int main(void) {
    
    DDRB |= ((1<<PB2)|(1<<PB3));
    PORTB &= ~((1<<PB2)|(1<<PB3));

    for(;;){
        PORTB |= (1<<PB2);
        PORTB &= ~(1<<PB3);
        _delay_ms(1000);
        PORTB &= ~(1<<PB2);
        PORTB |= (1<<PB3);
        _delay_ms(1000);
    }
    return 0;
}
