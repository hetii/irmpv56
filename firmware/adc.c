#include "adc.h"

uint8_t get_adc_button(void)
{
    // ADC = (Vin/VAREF) x 1024 
    // 3,51 => 784
    // 4,58 => 1024
    // 4,14 => 925
    // 3,79 => 847
    
    // 3,52 => 800
    // 4,58 => 1024.2
    // 4,16 => 946
    // 3,81 => 866

    uint16_t adcv = 0;
    ADCSRA |= (1<<ADSC);
    while (ADCSRA & (1 << ADSC));
    adcv = ADC;

    if (adcv >= 990){
        return 1;
    } else if (adcv >= 840 && adcv <= 870){
        return 2;      
    } else if (adcv >= 920 && adcv <= 960){
        return 3;
    }
    return 0;
}

void adc_init(void)
{
    DDRC &=~ (1<<PC1);
    // AVCC to 5v and channel 1.
    ADMUX  =  ((1<<REFS0) | (1<<MUX0)); 
    // Enable ADC and set prescaler to 128.
    ADCSRA =  ((1<<ADEN) | (1<<ADPS0)|(1<<ADPS1)|(1<<ADPS2)); 
}
