#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include "irmp.h"
#include "irsnd.h"
#include "twi.h"
#include "adc.h"

#define IR_LED_PORT PORTB
#define IR_LED_DDR DDRB
#define IR_LED_PIN 1
static uint8_t power_state = 0;
/*
Power on/off   : protocol: 0x02   NEC   address: 0xBF00   command: 0x0000
Mute           : protocol: 0x02   NEC   address: 0xBF00   command: 0x0001
1              : protocol: 0x02   NEC   address: 0xBF00   command: 0x0002
2              : protocol: 0x02   NEC   address: 0xBF00   command: 0x0003
3              : protocol: 0x02   NEC   address: 0xBF00   command: 0x0004
4              : protocol: 0x02   NEC   address: 0xBF00   command: 0x0005
5              : protocol: 0x02   NEC   address: 0xBF00   command: 0x0006
6              : protocol: 0x02   NEC   address: 0xBF00   command: 0x0007
7              : protocol: 0x02   NEC   address: 0xBF00   command: 0x0008
8              : protocol: 0x02   NEC   address: 0xBF00   command: 0x0009
9              : protocol: 0x02   NEC   address: 0xBF00   command: 0x000A
-/--           : protocol: 0x02   NEC   address: 0xBF00   command: 0x000B
0              : protocol: 0x02   NEC   address: 0xBF00   command: 0x000C
-><-           : protocol: 0x02   NEC   address: 0xBF00   command: 0x000D
Vol +          : protocol: 0x02   NEC   address: 0xBF00   command: 0x0048
Vol -          : protocol: 0x02   NEC   address: 0xBF00   command: 0x0049
DSP            : protocol: 0x02   NEC   address: 0xBF00   command: 0x0040
CH +           : protocol: 0x02   NEC   address: 0xBF00   command: 0x004A
CH -           : protocol: 0x02   NEC   address: 0xBF00   command: 0x004B
Input          : protocol: 0x02   NEC   address: 0xBF00   command: 0x000F
Zoom           : protocol: 0x02   NEC   address: 0xBF00   command: 0x0044
Sleep          : protocol: 0x02   NEC   address: 0xBF00   command: 0x0043
Up             : protocol: 0x02   NEC   address: 0xBF00   command: 0x0013
Left           : protocol: 0x02   NEC   address: 0xBF00   command: 0x0011
Enter          : protocol: 0x02   NEC   address: 0xBF00   command: 0x0010
Right          : protocol: 0x02   NEC   address: 0xBF00   command: 0x0012
Down           : protocol: 0x02   NEC   address: 0xBF00   command: 0x0014
Exit           : protocol: 0x02   NEC   address: 0xBF00   command: 0x0015
Menu           : protocol: 0x02   NEC   address: 0xBF00   command: 0x0016
Pmode          : protocol: 0x02   NEC   address: 0xBF00   command: 0x0041
Smode          : protocol: 0x02   NEC   address: 0xBF00   command: 0x0042
Auto           : protocol: 0x02   NEC   address: 0xBF00   command: 0x0047
Angle          : protocol: 0x02   NEC   address: 0xBF00   command: 0x005B
|<<            : protocol: 0x02   NEC   address: 0xBF00   command: 0x0054
>>|            : protocol: 0x02   NEC   address: 0xBF00   command: 0x0055
<<             : protocol: 0x02   NEC   address: 0xBF00   command: 0x0056
>>             : protocol: 0x02   NEC   address: 0xBF00   command: 0x0057
Eject ^        : protocol: 0x02   NEC   address: 0xBF00   command: 0x0050
Play/Pause >|| : protocol: 0x02   NEC   address: 0xBF00   command: 0x0051
Stop           : protocol: 0x02   NEC   address: 0xBF00   command: 0x0052
||>            : protocol: 0x02   NEC   address: 0xBF00   command: 0x0053
RED/Setup      : protocol: 0x02   NEC   address: 0xBF00   command: 0x004C
GREEN/Menu     : protocol: 0x02   NEC   address: 0xBF00   command: 0x004D
YELLOW/Title   : protocol: 0x02   NEC   address: 0xBF00   command: 0x004E
BLUE/Lang      : protocol: 0x02   NEC   address: 0xBF00   command: 0x004F
*/

static void timer1_init (void) {
    OCR1A  =  (F_CPU / F_INTERRUPTS) - 1;                                  // compare value: 1/15000 of CPU frequency
    TCCR1B = (1 << WGM12) | (1 << CS10);                                   // switch CTC Mode on, set prescaler to 1
    #ifdef TIMSK1                                                          // OCIE1A: Interrupt by timer compare
        TIMSK1 = 1 << OCIE1A;
    #else
        TIMSK  = 1 << OCIE1A;
    #endif
}

volatile uint16_t usecond_timer = 0;
volatile uint8_t second_timer = 0;

// Timer1 output compare A interrupt service routine, called every 1/15000 sec
ISR(TIMER1_COMPA_vect){
  if (! irsnd_ISR()){
    (void) irmp_ISR();
  }
  if (usecond_timer++ >= 15000){
    usecond_timer = 0;
    if (second_timer > 0){
        second_timer--;
    }
  }
}

static void send_ir(uint8_t protocol, uint16_t address, uint16_t command, uint8_t flags){
    IRMP_DATA irmp_data;
    irmp_data.protocol = protocol;
    irmp_data.address  = address;
    irmp_data.command  = command;
    irmp_data.flags    = flags;
    // Send frame, wait for completion:
    irsnd_send_data (&irmp_data, TRUE);
}

static void ir_led_callback (uint8_t on){
    // As we connect this avr directly to input of v56 chip
    // We don`t need to use carrier sygnal and instead we use it unmodulated.
    //
    // If you plan to change that please set IRSND_USE_CALLBACK set to "0" in irsndconfig.h
    // and use pin IRSND_OCx instead this method.
    if (on){
       IR_LED_PORT &= ~(1 << IR_LED_PIN);
    } else {
       IR_LED_PORT |= (1 << IR_LED_PIN);
    }
}

static void hardware_init(void){
    IR_LED_DDR |= (1 << IR_LED_PIN);  // LED pin to output
    IR_LED_PORT |= (1 << IR_LED_PIN); // switch LED off (active low)
    
    // PD5 connected to v56 backlight on (+5v)
    DDRD &= ~(1<<PD5); 
    PORTD &= ~(1<<PD5);
}

static void backlight_on(void){   
    uint8_t tx[2];
    tx[0] = 0x01;
    tx[1] = 0x05;
    twi_write(0x2C, &tx[0], 2);
    _delay_ms(70);
}

static void backlight_off(void){
    uint8_t tx[2];
    tx[0] = 0x01;
    tx[1] = 0x00;
    twi_write(0x2C, &tx[0], 2);
    _delay_ms(70);
}

static void backlight_set(uint8_t level){
    uint8_t tx[2] = {};
    tx[0] = 0x00;
    tx[1] = level;
    twi_write(0x2C, &tx[0], 2);
    _delay_ms(70);
}

static void backlight_blink(void){
    _delay_ms(200);
    backlight_off();
    _delay_ms(200);
    backlight_on();
    _delay_ms(200);
    backlight_off();
    _delay_ms(200);
    backlight_on();
}

static void backlight_up_down(uint8_t up){
    static uint8_t backlight_level = 0;
    uint8_t backlight_step = 1;
    
    if (backlight_level > 30){
        backlight_step = 2;
    } else if (backlight_level > 60){
        backlight_step = 4;
    } else if (backlight_level > 120){
        backlight_step = 10;
    } else if (backlight_level > 240){
        backlight_step = 1;
    }

    if (up == 1){
        if ((backlight_level + backlight_step) > 254){
            backlight_level = 254;
            backlight_blink();
        } else {
            backlight_level = backlight_level + backlight_step;
        }
    } else {
        if ((backlight_level - backlight_step) < 3){
            backlight_level = 3;
            backlight_blink();
        } else {
            backlight_level = backlight_level - backlight_step;
        }
    }
    backlight_set(backlight_level);
}

void check_power_state(void){
    
    static uint8_t old_power_state = 100;
    if (PIND & (1<<PD5)) {
        power_state = 1;
    } else {
        power_state = 0;
    }

    if (old_power_state != power_state){
        old_power_state = power_state;
        if (power_state == 1) {
            backlight_on();
        } else {
            backlight_off();
        }
    }
}

int main(void) {
    IRMP_DATA irmp_data;
    uint8_t button = 0;

    hardware_init();
    adc_init();   
    twi_init();
    irmp_init();
    irsnd_init();
    timer1_init();
    irsnd_set_callback_ptr (ir_led_callback);

    sei();
    backlight_set(254);

    wdt_enable(WDTO_2S);
    uint8_t menu = 0;
    uint8_t old_button = 0;
    
    for(;;){
        wdt_reset();
        check_power_state();

        button = get_adc_button();
        if (button > 0 && power_state == 0){
            menu = 0;
            send_ir(0x02, 0xBF00, 0x0000, 0); // Power on
            _delay_ms(2000);
        }

        if (button == 1){
            _delay_ms(250);
            if (menu > 4){
                menu = 0;
            }
            menu++;

            if (menu == 2){
                send_ir(0x02, 0xBF00, 0x000F, 0); // Input
            }

            /*if (menu == 0){
                send_ir(0x02, 0xBF00, 0x0048, 0); // Vol +
                _delay_ms(250);
                send_ir(0x02, 0xBF00, 0x0049, 0); // Vol -
            } else if (menu == 1){
                send_ir(0x02, 0xBF00, 0x004A, 0); // CH +
                _delay_ms(250);
                send_ir(0x02, 0xBF00, 0x004B, 0); // CH -
            } else if (menu == 2){
                send_ir(0x02, 0xBF00, 0x000F, 0); // Input
                _delay_ms(400);
            } else if (menu == 3){
                backlight_set(100);
                _delay_ms(200);
                backlight_set(244);
                _delay_ms(200);
                backlight_set(100);
            }*/
        } else if (button == 2){
            if (menu == 0){
                send_ir(0x02, 0xBF00, 0x0048, 0);
            } else if (menu == 1){
                send_ir(0x02, 0xBF00, 0x004A, 0);
            } else if (menu == 2){
            //    send_ir(0x02, 0xBF00, 0x000F, 0); // Input
                send_ir(0x02, 0xBF00, 0x0013, 0); // Up
            } else if (menu == 3){
                backlight_up_down(1);
            }
        } else if (button == 3){
            if (menu == 0){
                send_ir(0x02, 0xBF00, 0x0049, 0);
            } else if (menu == 1){
                send_ir(0x02, 0xBF00, 0x004B, 0);
            } else if (menu == 2){
            //  send_ir(0x02, 0xBF00, 0x000F, 0); // Input
                send_ir(0x02, 0xBF00, 0x0014, 0); // Down
            } else if (menu == 3){
                backlight_up_down(0);
            }
        }

        if (irmp_get_data (&irmp_data)) {
            irmp_data.flags = 0x0;
            if (irmp_data.protocol == 0x02 && irmp_data.address == 0xBF00){
                // Resend original remote IR codes:
                irsnd_send_data (&irmp_data, TRUE);
                // Additional logic can be added here:
                // if (irmp_data.command == 0x0000){}
            }
        }
    }
    return 0;
}
