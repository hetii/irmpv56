#!/usr/bin/env python
import sys
import os
import binascii


def main():
    """ This program convert data from i2cdump and convert it to binary form:
        
        1. i2cdump -y 17 0x50 b > model_of_your_screen.hex
        2. i2cdump2bin.py model_of_your_screen.hex model_of_your_screen.bin
        3. cat model_of_your_screen.bin | edid-decode > model_of_your_screen.txt
    """
    if len(sys.argv) < 3 or not os.path.exists(sys.argv[1]):
        print(main.__doc__)
        return

    with open(sys.argv[1]) as fin, open(sys.argv[2], 'wb') as fout:
        for i, line in enumerate(fin):
            if i is 0:
                continue
            l = line[4:51]
            fout.write(binascii.unhexlify(''.join(l.split())))
    print("All fine - if you wish you can do now: cat %s | edid-decode" % sys.argv[2])


if __name__ == '__main__':
    main()
